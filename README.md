# **DDoS Service**

[[_TOC_]]

## Idea
 Distributed Denial of Service (DDoS) attacks are one of the most significant and common attacks taking place digitally on businesses. In the second half of 2020 alone, more than ten million attacks were registered, according to a report by Netscout. This represents a 20% increase in attacks compared to the same period last year. (cf. NETSCOUT p. 5)

### Problem
DDoS attacks are attacks on victims' servers. The availability of the systems is restricted by sending requests to them in such numbers and loads that the systems have to be throttled or temporarily disconnected completely from external requests. The resulting damage varies in type and amount from company to company. Not to be neglected, however, are attacks on institutions such as hospitals. Server failures can lead to the loss of human life.

### Solution
The goal of this project is to create a way for companies to detect systems that pose a threat. To do this, it is necessary to stop communication with the attacking systems as early as possible. The following describes an approach with which this is possible.

Firewalls serve as the first instance of digital security to defend against attacks of various kinds. The system presented in this project complements firewalls by communicating with an API. This uses machine learning models to determine whether a request is benign or malicious.

## System Structure
In the following, the architecture and the structure of the databases are described. In addition, the procedure for a new classification is presented and the process for creating the machine learning models is described.

### Architecture
![System Architecture](/business/structure.jpg "System Architecture")

The figure shows the architecture for the project. The dashed lines represent parts of the whole system that are not part of the project.

PostgresSQL is used as the database management system (DBMS) due to its popularity and prior experience. Two databases are defined. The structure of the database essential for the product is depicted in the figure below. The other one is used to store monitoring data. For documentation of these please use https://flask-monitoringdashboard.readthedocs.io/en/latest/.

![DDoS Service Database Schema](/business/pgadmin_ddos_database.png "DDoS Service Database Schema")

A separate API serves as the interface to the firewalls, which was created using Python and Flask oriented to the REST standard. The Object-Relational-Mapping (ORM) was realized with SQLAlchemy. The advantage is the easy connection with the ML models, which were created with the scikit-learn library.

Another API is available exclusively for the frontend of the monitoring dashboard, through which staff or further systems have the possibility to monitor the communication with the service API.

### Sample Process
![Classification Process](/business/new_ip_process.jpg "Classification Process")

The figure shows an example flow of a new classification. First, the monitoring interface collects the metadata about the communication and stores information such as the IP and the timestamp. A check is made to see if a valid token is present. If so, the data is transformed using a predefined pipeline that makes necessary adjustments to the data for further use with the models.

After the transformation, the data is passed to various models (in this case, Decision Tree and a neural network). These provide a probability for the presence of a DDoS attack. The scores of the individual models are then weighted using the mean value. Even though the models already have a high accuracy, this weighting serves to reduce false evaluations.

The classification result is stored with the sent data set and then returned.

### Model Creation
The data for the creation of the models come from the following address: https://www.kaggle.com/devendra416/ddos-datasets. These are simulated data from data sets of different years, since the availability of real data is not available due to data protection conditions, among other things. These were combined into individual communications using the CIC-FlowMeter-v4. The resulting and used dataset includes 84 features and more than 7 million entries. It is worth mentioning that the incoming data need the same structure to be classified by the models. (cf. Prasad et al.).

The data were then cleaned, analyzed, and transformed in three steps. These included standardization of the data, principal component analysis (PCA) for the continuous data, and one-hot encoding (OHC) for the discrete data.

For model building, a total of 8 different models were trained on a reduced data set, 2 of which were selected for use in the API. The two models selected are a decision tree and a neural network.

## Documentation
Below are instructions on how to set up and use the project, and documentation of the API's interfaces.

### Instructions
The files for the project can be accessed at https://gitlab.com/Ronho1/ddos-service. However, only the file "docker-compose.yml" is required for the installation. Additionally, Docker is required. Installation instructions and Docker itself are on the official website (https://www.docker.com/products/docker-desktop). Once Docker is installed, a terminal must be opened in the folder of "docker-compose.yml" and "docker compose up" must be executed. The image of the service, the database and the database administration software pgAdmin are now downloaded and a new instance is created. This process can take some time, as test data is also generated the first time, which is necessary for access.

Once the instance is created and running, the API of the service can be accessed via "localhost:5000/v1" in the browser. Here, interactive Swagger documentation can be found.
The dashboard for monitoring communication can be accessed with "localhost:5000/dashboard". The password "admin" and the username "admin" are required for access. Analyses of different interfaces and time periods can be found.

The database can be accessed via the supplied pgAdmin. For this, "localhost:8080" must be called in the browser. Then the user name "admin@linuxhint.com" and the password "secret" must be entered.

![pgAdmin Login](/business/pgadmin_login.png "pgAdmin Login")

After that the server must be selected in the now opened window (click the "Servers" button in the left area). Then a new connection must be established via Object - Create - Server... .

![pgAdmin create new server](/business/pgadmin_new_server.png "pgAdmin create new server")

In the new window the name "postgres" is defined and in the tab "Connection" the hostname "db", the port "5432", the maintenance database "postgres", the username "root" and the password "1234" are entered. The "Save" button is used to confirm the settings and establish the connection to the databases. For more information on how to use pgAdmin, please refer to the official documentation at https://www.pgadmin.org/docs/.

_ | _
:-------------------------:|:-------------------------:
![pgAdmin new server general settings](/business/pgadmin_new_server_general.png "pgAdmin new server general settings")  |  ![pgAdmin new server connection settings](/business/pgadmin_new_server_connection.png "pgAdmin new server connection settings")

### DDoS Service Endpoints
**user:**
| Endpoint                       | Method     | Description                                   |
| ------------------------------ | ---------- | --------------------------------------------- |
| /user/company                  | get        | get company of user by token (jwt)            |
| /user/signin                   | post       | signin with user credentials                  |
| /user/signup                   | post       | create new user                               |
| /user/update                   | post       | update names of a user                        |
| /user/update                   | get        | get updatable fields of an user               |
| /user/verify-token             | post       | verify that the token is valid                |

**company:**
| Endpoint                       | Method     | Description                                   |
| ------------------------------ | ---------- | --------------------------------------------- |
| /company/list                  | get        | get list of all registered companies          |
| /company/register              | post       | register a new company<sup>1</sup>            |
| /company/{company}             | get        | get all users of a company                    |

<sup>1</sup>This endpoint is only for illustrative purposes. In a real environment, it is not recommend to have an unprotected endpoint for creating a company

**ip:**
| Endpoint                       | Method     | Description                                   |
| ------------------------------ | ---------- | --------------------------------------------- |
| /ip/                           | post       | post new record, returns classification       |
| /ip/blacklist                  | get        | list of possible harmful ip addresses         |
| /ip/whitelist                  | get        | list of possible harmless ip addresses        |
| /ip/{ip}                       | get        | get class counts of last 10 entries of ip     |
| /ip/{ip}/status                | get        | get current status of ip                      |

### Use of the Swagger Documentation
To open the api, enter "localhost:5000/v1" in your browser. You should now see the swagger documentation as in the picture below.

![Swagger API](/business/swagger_api.png "Swagger API")

Most of the routes are protected which is indicated by the "Authorization" field after opening a route:

![Swagger API IP Blacklist Endpoint](/business/swagger_api_ip_blacklist.png "Swagger API IP Blacklist Endpoint")

To get a token which we can fill the field with, we need to sign in. Open the users and signin endpoint. Click on "Try it out" and enter the following credentials:
- email: max@gmail.com
- password: Passwort123!

It should look like this:

![Swagger API IP Users Signin Endpoint](/business/swagger_api_user_signin.png "Swagger API IP Users Signin Endpoint")

Now, click on execute. The result is depicted below. You can copy the token from the authorization header. 

![Swagger API IP Users Signin Endpoint Success Response](/business/swagger_api_user_signin_success_response.png "Swagger API IP Users Signin Endpoint Success Response")

Insert it in the "Authorization" fields where needed, i.e. the blacklist endpoint from the beginning:

![Swagger API IP Blacklist Endpoint With Token Inserted](/business/swagger_api_ip_blacklist_with_token.png "Swagger API IP Blacklist Endpoint With Token Inserted")

After execution, we get the result. We can see all ips that are currently blacklisted by the service.

![Swagger API IP Blacklist Endpoint Success Response](/business/swagger_api_ip_blacklist_success_response.png "Swagger API IP Blacklist Endpoint Success Response")

Go ahead and test out some more endpoints. Since the product focuses on the classification of new entries, you can get some sample data in the next section.

### Example Data for the '/ip/' endpoint

The following entry results in a 'ddos' classification:
```json
[
    {
    "Flow ID": "172.31.69.25-18.219.193.20-80-53836-6",
    "Src IP": "18.219.193.20",
    "Src Port": 53836,
    "Dst IP": "172.31.69.25",
    "Dst Port": 80,
    "Protocol": 6,
    "Timestamp": "16/02/2018 11:23:48 PM",
    "Flow Duration": 50547,
    "Tot Fwd Pkts": 2,
    "Tot Bwd Pkts": 1,
    "TotLen Fwd Pkts": 0,
    "TotLen Bwd Pkts": 0,
    "Fwd Pkt Len Max": 0,
    "Fwd Pkt Len Min": 0,
    "Fwd Pkt Len Mean": 0,
    "Fwd Pkt Len Std": 0,
    "Bwd Pkt Len Max": 0,
    "Bwd Pkt Len Min": 0,
    "Bwd Pkt Len Mean": 0,
    "Bwd Pkt Len Std": 0,
    "Flow Byts/s": 0,
    "Flow Pkts/s": 59.350703305834166,
    "Flow IAT Mean": 25273.5,
    "Flow IAT Std": 22787.930237298868,
    "Flow IAT Max": 41387,
    "Flow IAT Min": 9160,
    "Fwd IAT Tot": 9160,
    "Fwd IAT Mean": 9160,
    "Fwd IAT Std": 0,
    "Fwd IAT Max": 9160,
    "Fwd IAT Min": 9160,
    "Bwd IAT Tot": 0,
    "Bwd IAT Mean": 0,
    "Bwd IAT Std": 0,
    "Bwd IAT Max": 0,
    "Bwd IAT Min": 0,
    "Fwd PSH Flags": 0,
    "Bwd PSH Flags": 0,
    "Fwd URG Flags": 0,
    "Bwd URG Flags": 0,
    "Fwd Header Len": 64,
    "Bwd Header Len": 32,
    "Fwd Pkts/s": 39.56713553722278,
    "Bwd Pkts/s": 19.78356776861139,
    "Pkt Len Min": 0,
    "Pkt Len Max": 0,
    "Pkt Len Mean": 0,
    "Pkt Len Std": 0,
    "Pkt Len Var": 0,
    "FIN Flag Cnt": 0,
    "SYN Flag Cnt": 0,
    "RST Flag Cnt": 0,
    "PSH Flag Cnt": 0,
    "ACK Flag Cnt": 1,
    "URG Flag Cnt": 0,
    "CWE Flag Count": 0,
    "ECE Flag Cnt": 0,
    "Down/Up Ratio": 0,
    "Pkt Size Avg": 0,
    "Fwd Seg Size Avg": 0,
    "Bwd Seg Size Avg": 0,
    "Fwd Byts/b Avg": 0,
    "Fwd Pkts/b Avg": 0,
    "Fwd Blk Rate Avg": 0,
    "Bwd Byts/b Avg": 0,
    "Bwd Pkts/b Avg": 0,
    "Bwd Blk Rate Avg": 0,
    "Subflow Fwd Pkts": 2,
    "Subflow Fwd Byts": 0,
    "Subflow Bwd Pkts": 1,
    "Subflow Bwd Byts": 0,
    "Init Fwd Win Byts": -1,
    "Init Bwd Win Byts": 225,
    "Fwd Act Data Pkts": 0,
    "Fwd Seg Size Min": 0,
    "Active Mean": 0,
    "Active Std": 0,
    "Active Max": 0,
    "Active Min": 0,
    "Idle Mean": 0,
    "Idle Std": 0,
    "Idle Max": 0,
    "Idle Min": 0
    }
]
```

The following entry results in a 'benign' classification:
```json
[
    {
    "Flow ID": "172.31.68.19-36.70.157.168-445-10497-6",
    "Src IP": "36.70.157.168",
    "Src Port": 10497,
    "Dst IP": "172.31.68.19",
    "Dst Port": 445,
    "Protocol": 6,
    "Timestamp": "20/02/2018 05:22:05",
    "Flow Duration": 239941,
    "Tot Fwd Pkts": 3,
    "Tot Bwd Pkts": 1,
    "TotLen Fwd Pkts": 0,
    "TotLen Bwd Pkts": 0,
    "Fwd Pkt Len Max": 0,
    "Fwd Pkt Len Min": 0,
    "Fwd Pkt Len Mean": 0,
    "Fwd Pkt Len Std": 0,
    "Bwd Pkt Len Max": 0,
    "Bwd Pkt Len Min": 0,
    "Bwd Pkt Len Mean": 0,
    "Bwd Pkt Len Std": 0,
    "Flow Byts/s": 0,
    "Flow Pkts/s": 16.670764896370358,
    "Flow IAT Mean": 79980.33333333334,
    "Flow IAT Std": 138457.26087617554,
    "Flow IAT Max": 239857,
    "Flow IAT Min": 1,
    "Fwd IAT Tot": 239941,
    "Fwd IAT Mean": 119970.5,
    "Fwd IAT Std": 169662.49397111902,
    "Fwd IAT Max": 239940,
    "Fwd IAT Min": 1,
    "Bwd IAT Tot": 0,
    "Bwd IAT Mean": 0,
    "Bwd IAT Std": 0,
    "Bwd IAT Max": 0,
    "Bwd IAT Min": 0,
    "Fwd PSH Flags": 0,
    "Bwd PSH Flags": 0,
    "Fwd URG Flags": 0,
    "Bwd URG Flags": 0,
    "Fwd Header Len": 72,
    "Bwd Header Len": 32,
    "Fwd Pkts/s": 12.503073672277768,
    "Bwd Pkts/s": 4.167691224092589,
    "Pkt Len Min": 0,
    "Pkt Len Max": 0,
    "Pkt Len Mean": 0,
    "Pkt Len Std": 0,
    "Pkt Len Var": 0,
    "FIN Flag Cnt": 0,
    "SYN Flag Cnt": 0,
    "RST Flag Cnt": 0,
    "PSH Flag Cnt": 1,
    "ACK Flag Cnt": 0,
    "URG Flag Cnt": 0,
    "CWE Flag Count": 0,
    "ECE Flag Cnt": 0,
    "Down/Up Ratio": 0,
    "Pkt Size Avg": 0,
    "Fwd Seg Size Avg": 0,
    "Bwd Seg Size Avg": 0,
    "Fwd Byts/b Avg": 0,
    "Fwd Pkts/b Avg": 0,
    "Fwd Blk Rate Avg": 0,
    "Bwd Byts/b Avg": 0,
    "Bwd Pkts/b Avg": 0,
    "Bwd Blk Rate Avg": 0,
    "Subflow Fwd Pkts": 3,
    "Subflow Fwd Byts": 0,
    "Subflow Bwd Pkts": 1,
    "Subflow Bwd Byts": 0,
    "Init Fwd Win Byts": 8192,
    "Init Bwd Win Byts": 8192,
    "Fwd Act Data Pkts": 0,
    "Fwd Seg Size Min": 20,
    "Active Mean": 0,
    "Active Std": 0,
    "Active Max": 0,
    "Active Min": 0,
    "Idle Mean": 0,
    "Idle Std": 0,
    "Idle Max": 0,
    "Idle Min": 0
    }
]
```

## Sources
>NETSCOUT. “ISSUE 6: FINDINGS FROM 2H 2020: NETSCOUT THREAT INTELLIGENCE REPORT Including the 16th annual Worldwide Infrastructure Security Report (WISR).” Edited by Richard Hummel, et al. DDoS in a Time of Pandemic. https://www.netscout.com/sites/default/files/2021-04/ThreatReport_2H2020_FINAL_0.pdf.

>Prasad, M. Devendra, et al. Machine Learning DDoS Detection Using Stochastic Gradient Boosting. Bangalore, 2019. International Journal of Computer Sciences and Engineering, https://www.ijcseonline.org/pdf_paper_view.php?paper_id=4011&28-IJCSE-06600.pdf. Accessed 02 08 2021.
